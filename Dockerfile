FROM python:3.9.10

WORKDIR /fetch
COPY . .

SHELL ["/bin/bash","-c"]

RUN pip install -r requirements.txt
RUN mv fetch.py fetch
RUN chmod +x ./fetch

CMD [ "/bin/bash" ]