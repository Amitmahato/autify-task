#!/usr/local/bin/python

from bs4 import BeautifulSoup
from urllib.parse import urlparse
import xattr
import time, os, requests, argparse

OUTPUT_DIRECTORY = "./websites/"

def encodeToBinary(data) -> bytes:
    return str(data).encode("ascii")

def decodeBinaryToString(data:bytes) -> str:
    return data.decode("ascii")

def setFileMetaData(filePath, metadata:dict):
    for key, value in metadata.items():
        xattr.setxattr((filePath),"user."+str(key),encodeToBinary(value))

def getFileMetaData(filePath) -> dict:
    metadata = {}
    for userAttr in xattr.listxattr(filePath):
        attr = userAttr.split(".")[1]
        metadata[attr] = decodeBinaryToString(xattr.getxattr(filePath,userAttr))
    return metadata

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--metadata', nargs="+", default=None, help='get file meta data for provided list of urls')
    parser.add_argument("urls", nargs="*", help="list of urls with http:// or https:// scheme")
    args = parser.parse_args()

    if args.metadata:
        for url in args.metadata:
            # construct file name to get the metadata for the saved website
            site = urlparse(url).netloc
            filePath = OUTPUT_DIRECTORY + site + ".html"

            if os.path.exists(filePath):
                metadata = getFileMetaData(filePath)
                for attr, value in metadata.items():
                    print(attr+": ",value)
                print("")
            else:
                print("File doesn't exist for url: ",url,"\n")
    else:
        for url in args.urls:
            try:
                # get website content for the given url and prettify the content
                page = requests.get(url)
                webpage = BeautifulSoup(page.content, "html.parser")

                # make a directory if not exists to store the files generated
                directoryExist = os.path.exists(OUTPUT_DIRECTORY)
                if not directoryExist:    
                    os.mkdir(OUTPUT_DIRECTORY)

                # construct file name to save the website content into it
                site = urlparse(url).netloc
                filePath = OUTPUT_DIRECTORY + site + ".html"

                # open the file in write mode and dump the website content into this file and close the file
                file = open(filePath, mode="w+")
                
                file.writelines(webpage.prettify())
                
                last_fetch = time.ctime()
                num_link = len(webpage.find_all("a"))
                images = len(webpage.find_all("img"))

                metadata = {
                    "site": site,
                    "num_link": num_link,
                    "images": images,
                    "last_fetch": last_fetch,
                }

                setFileMetaData(filePath,metadata)

                file.close()

            except Exception as e:
                print("Error: ",e)