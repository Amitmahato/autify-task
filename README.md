# Restriction & Guideline

- Choose any programming language other than bash.
- Do not invoke unix utilities (i.e. curl, wget, etc)
- Add a Dockerfile that can build and run the executable

# Problem

Please implement a command line program that can fetch web pages and saves them to disk for later retrieval and browsing.

### Section 1

For example if we invoked the program like: `./fetch https://www.google.com` then in our current directory we should have a file containing the contents of www.google.com. (i.e. `/home/myusername/fetch/www.google.com.html`).

We should be able to specify as many urls as we want:

```jsx
$> ./fetch https://www.google.com https://autify.com <...>
$> ls
autify.com.html www.google.com.html
```

If the program runs into any errors while downloading the html it should print the error to the console.

### Section 2

Record metadata about what was fetched:

- What was date and time of last fetch
- How many links are on the page
- How many images are on the page

Modify the script to print this metadata.

For example (it can work differently if you like)

```jsx
$> ./fetch --metadata https://www.google.com
site: www.google.com
num_links: 35
images: 3
last_fetch: Tue Mar 16 2021 15:46 UTC
```

## Extra credit

Archive all assets as well as html so that you load the web page locally with all assets loading properly.

# Solution

## Tools & Environment

- Programming Language: `Python`
- Dependency:
  - `requests`
  - `beautifulsoup4`
  - `xattr`
- Docker Image: `python:3.9.10`

## How to run commands in docker?

- First build image using `sudo docker image build -t fetch .`
- Second run the container in interactive mode using `docker container run -it --rm fetch` & you will get a bash shell
- Now running `./fetch https://youtube.com https://autify.com https://google.com` will generate corresponding html file inside `websites` directory
- Running `./fetch --metadata https://youtube.com https://autify.com https://google.com` will display the corresponding files metadata
